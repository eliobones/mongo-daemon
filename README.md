![](https://elioway.gitlab.io/eliobones/mongo-daemon/elio-mongo-daemon-logo.png)

> MongoDb in the development environment, **the elioWay**

# mongo-daemon ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

Docker Compose ready MondoDb-Daemon for expressjs/mongoosejs apps.

- [mongo-daemon Documentation](https://elioway.gitlab.io/eliobones/mongo-daemon/)

## Installing

- [Installing mongo-daemon](https://elioway.gitlab.io/eliobones/mongo-daemon/installing.html)

## Requirements

- [mongo-daemon Prerequisites](https://elioway.gitlab.io/eliobones/mongo-daemon/prerequisites.html)

## Seeing is Believing

```
minikube start
cd elioway/eliobones/mongo-daemon
docker-compose up
```

- [eliobones Quickstart](https://elioway.gitlab.io/eliobones/quickstart.html)
- [mongo-daemon Quickstart](https://elioway.gitlab.io/eliobones/mongo-daemon/quickstart.html)

# Credits

- [mongo-daemon Credits](https://elioway.gitlab.io/eliobones/mongo-daemon/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/eliobones/mongo-daemon/apple-touch-icon.png)
