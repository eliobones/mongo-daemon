"use strict"
require("dotenv").config()
const express = require("express")
const mongoose = require("mongoose")
const database = require("./database.js")

const app = express()

let { MONGODB_URL } = process.env
let connected = false
let port = 3030
console.log(`The MongoDb-Daemon will respond to ${MONGODB_URL}.`)

database
  .dbconnect()
  .on("error", (err) => console.log({ err }))
  .on("connected", (stream) => {
    connected = true
  })

app.get("/", (req, res) => {
  let readyState = connected
    ? "is awake and hungry."
    : "doesn't want to play. Come back later. "
  res.send(`The MongoDb-Daemon ${readyState}`)
})

app.listen(port, () =>
  console.log(`\`curl http://localhost:${port}/\` to poke the MongoDb-Daemon.`)
)
