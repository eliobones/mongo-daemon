"use strict"
require("dotenv").config()
const mongoose = require("mongoose")

let { MONGODB_URL } = process.env

function dbconnect() {
  mongoose
    .connect("mongodb://127.0.0.1:27017/", { useNewUrlParser: true })
    .then(() => console.log("MongoDB Connected"))
    .catch((ex) => console.log({ ex }))
  return mongoose.connection
}

function dbclose() {
  return mongoose.disconnect()
}

module.exports = {
  dbconnect,
  dbclose,
}
