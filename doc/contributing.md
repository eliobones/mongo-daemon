# Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/mongo-daemon.git
cd mongo-daemon
git clone https://gitlab.com/eliobones/mongo-daemon.git
```

- [Quickstart daemon](/elioangels/daemon/quickstart.html)

```shell
# wake it up?
docker version --format "v{{.Client.Version}}"
minikube start
```

```shell
cd path/to/elioapp
```

- [mongo-daemon Recipe](/eliobones/mongo-daemon/recipe.html)

```shell
docker-compose up
```

- [Cheat docker-compose](/elioangels/daemon/cheat-docker-compose.html)

## TODOS

1. TODOS
