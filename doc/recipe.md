# mongo-daemon Recipe

- [mongo-daemon Prerequisites](/eliobones/mongo-daemon/prerequisites.html)

This recipe shows you how to use the MongoDb-Daemon pattern to provision a MongoDb Docker container which your Mongoose/Express based app can connect to it. It was the recipe used to create **mondo-daemon** - so you don't need to do this (you're welcome).

## Nutshell

- Create a `docker-compose.yaml` file which describes the MongoDb service `mongo` and your `app` service whose `localhost` on port `27017` acts as a daemon to the `mongo` service provisioned.
- Create a `Dockerfile` which describes how to build and run a container for your app.
- Call `docker-compose up`
- Call `docker-compose down`

## Set up

```
npm init -y
npm i @elioway/bones express mongoose
npm i --save-dev nodemon prettier
```

Create a `hello-daemon.js` file.

This represents a dummy app for demo purposes. In your app you will point to your app's entry point, e.g. `server.js`, `index.js`, `app.js`, etc. In this example, `hello-daemon`, creates a microservice which you can `curl http://localhost@3030/` to see if the app was able to connect to the MongoDb container that Docker Compose will create.

```
echo 'const express = require("express")
const mongoose = require("mongoose")
const database = require("@elioway/bones/bones/database")

const app = express()

let connected = false

database
  .dbconnect("mongodb://mongo:27017/mongo-daemon")
  .on("error", err => console.log({ app: "err" }, err))
  .on("connected", stream => {
    connected = true
  })

app.get("/", (req, res) => {
  let readyState = connected
    ? "is awake and hungry."
    : " doesn't want to play. Come back later. "
  res.send(`The MongoDb-Daemon ${readyState}`)
})

app.listen(3030, () =>
  console.log(`curl http://localhost:${3030}/ to poke the MongoDb-Daemon.`)
)' > hello-daemon.js
```

Create a `docker-compose.yaml` file.

```
echo 'version: "3"
services:
  app:
    container_name: eliodaemon-mongo
    restart: always
    build: ./
    ports:
      - "3000:3000"
    volumes:
      - .:/app
      # - .:/app/node_modules
    links:
      - mongo
  mongo:
    container_name: mongo
    image: mongo
    ports:
      - "27017:27017"
    volumes:
      - mongodb:/data/db
      - mongodb_config:/data/configdb

volumes:
  mongodb:
  mongodb_config:
' > docker-compose.yaml
```

Create a `Dockerfile` file. You may need to adapt this to your app, for instance if you have a build of your app in the `dist` folder using perhaps **babel** as part of your build workflow.

```
echo '# pull a node image from docker hub
FROM node:10

# set the working dir to /app
WORKDIR /app

# copy package.json to the container
COPY package.json package.json

#  install package.json modules in container
RUN npm install

# copy everything to container /app
COPY . .

# expose port 3000 to mount it to another port in local machine
EXPOSE 3000

#  install nodemon for changes on the fly
RUN npm i -g nodemon


#  start server inside container
CMD [ "npm", "start" ]
'  > Dockerfile
```

Now you can use Docker Compose to see if this is working.

```
minikube start
docker-compose build
docker-compose up
docker-compose run
```

# Development

```
minikube start
docker-compose up
```
