<aside>
  <dl>
  <dd>Dreadful was the din</dd>
  <dd>Of hissing through the hall, thick swarming now</dd>
  <dd>With complicated monsters head and tail</dd>
</dl>
</aside>

Working implementation of a dummy expressjs/mongoosejs app served by Docker, with a docked MongoDb.
