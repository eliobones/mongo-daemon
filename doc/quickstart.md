# Quickstart mongo-daemon

- [mongo-daemon Prerequisites](/eliobones/mongo-daemon/prerequisites.html)
- [Installing mongo-daemon](/eliobones/mongo-daemon/installing.html)

```
minikube start
cd elioway/eliobones/mongo-daemon
docker-compose up
```
